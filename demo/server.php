<?php
require_once '../vendor/autoload.php';
new UI_DevOutput;

$host	= "0.0.0.0";
$port	= 8001;

class MockingEventServer extends \CeusMedia\SocketStream\Server{

	protected $listeners	= array();

	protected function handleRequest( $connection, $request ){
		$request	= json_decode( $request );
		switch( $request->name ){
			case 'registerListener':
				remark( "Adding Listener: ".json_encode( $request->scopes ) );
				$this->handleRegisterListener( $request );
				remark( "=> Listeners: ".count( $this->listeners ) );
				break;
			case 'unregisterListener':
				remark( "Removing Listener: ".json_encode( $request->scopes ) );
				$this->handleUnregisterListener( $request );
				remark( "=> Listeners: ".count( $this->listeners ) );
				break;
			default:
				$this->handleEvent( $connection, $request );
		}
//		error_log( print_m( $request, NULL, NULL, TRUE )."\n", 3, "server.log" );
	}

	protected function handleEvent( $connection, $request ){
		remark( "Event: ".$request->name );
		switch( $request->name ){
			case 'changeQuota':
				$response	= json_encode( array( 'set' => rand( 1, 5 ) * 1024 ) );
				return $response;
		}
	}

	protected function handleRegisterListener( $request ){
		$this->listeners[]	= (object) array(
			'uuid'		=> $request->uuid,
			'scopes'	=> $request->scopes,
		);
	}

	protected function handleUnregisterListener( $request ){
		$requestScopes	= (object) array_merge(
			array( 'name' => NULL, 'uuid' => NULL ),
			(array) $request->scopes
		);
		foreach( $this->listeners as $index => $listener ){
			$listenerScopes	= (object) array_merge(
				array( 'name' => NULL, 'uuid' => NULL ),
				(array) $listener->scopes
			);
			$byUuid	= $requestScopes->uuid && $requestScopes->uuid === $listenerScopes->uuid;
			$byName = $requestScopes->name && $requestScopes->name === $listenerScopes->name && !$listenerScopes->uuid;
			if( $byUuid || $byName ){
				unset( $this->listeners[$index] );
			}
		}
	}
}

error_reporting( E_ALL );
ini_set( 'display_errors', 'On' );
$server	= new MockingEventServer( $host, $port );
