<?php
require_once '../vendor/autoload.php';
new UI_DevOutput;

require_once '../src/Event/Request.php';
require_once '../src/Event/Response.php';
require_once '../src/Event/Client.php';
require_once '../src/Event/Listener.php';
require_once '../src/Event/Request/Model.php';
require_once '../src/Event/Response/Model.php';
require_once '../src/Event/Response/Callback.php';
require_once '../src/Event/Response/Listener.php';

class Test{

	public function run(){

		$client		= new \Event\Client();

		$listener	= new \Event\Listener();
		$listener->setName( 'listUpdated' );
		$function	= function( \Event\Response\Model $response ){
			remark( "Quota changed" );
		};
		$listener->setCallback( new \Event\Response\Callback( $function ) );
		$client->addListener( $listener );

		$request	= new \Event\Request( $client );
		$request->setName( 'changeQuota' );
		$function	= function( \Event\Response\Model $response ){
			remark( 'Received response:' );
			remark( ' - name: '.$response->getName() );
 			remark( ' - uuid: '.$response->getRequest()->getUuid() );
			remark( ' - load: '.print_m( $response->getData(), NULL, NULL, TRUE ) );
		};
		$request->listen( 'quotaChanged', new \Event\Response\Callback( $function ) );

		$client->sendRequest( $request );

		$responseRaw	= json_encode( array(
			'name'	=> 'quotaChanged',
			'event'	=> array(
				'name'		=> $request->getName(),
				'scopes'	=> $request->getScopes(),
				'uuid'		=> $request->getUuid(),
			),
			'status'	=> '200',
			'data'		=> array( 'payload' ),
		), JSON_PRETTY_PRINT );

		$client->receiveRawResponse( $responseRaw );
	}
}

$test	= new Test();
$test->run();
