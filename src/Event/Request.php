<?php
namespace Event;

class Request{

	protected $client;
	protected $model;

	public function __construct( \Event\Client $client ){
		$this->client	= $client;
		$this->model	= new \Event\Request\Model;
	}


	public static function create( \Event\Client $client ){
		return new static( $client );
	}


	public function getName(){
		return $this->model->getName();
	}

	public function getScopes(){
		return $this->model->getScopes();
	}

	public function getUuid(){
		return $this->model->getUuid();
	}

	public function serialize(){
		return json_encode( array(
			'uuid'		=> $this->model->getUuid(),
			'name'		=> $this->model->getName(),
			'scopes'	=> $this->model->getScopes(),
		), JSON_PRETTY_PRINT );
	}

	public function listen( $name, \Event\Response\Callback $callback ){
		$listener	= \Event\Response\Listener::create()
			->setName( $name )
			->setRequestEvent( $this->model )
			->setCallback( $callback );
		$this->client->addListener( $listener );
	}

	public function setName( $name ){
		$this->model->setName( $name );
		return $this;
	}

	public function setScopes( $scopes ){
		$this->model->setScopes( $scopes );
		return $this;
	}
}
