<?php
namespace Event\Request;
class Model{

	protected $name;
	protected $scopes	= array();
	protected $uuid;

	public function __construct(){
		$this->setUuid( \Alg_ID::uuid() );
	}

	public static function create(){
		return new static;
	}

	public function getName(){
		return $this->name;
	}

	public function getScopes(){
		return $this->scopes;
	}

	public function getUuid(){
		return $this->uuid;
	}

	public function setScopes( $scopes ){
		$this->scopes	= $scopes;
		return $this;
	}

	public function setName( $name ){
		$this->name	= $name;
		return $this;
	}

	public function setUuid( $uuid ){
		$this->uuid	= $uuid;
		return $this;
}
	}
