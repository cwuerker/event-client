<?php
namespace Event\Response;

class Listener extends \Event\Listener{

	protected $request;

	public function __construct(){}

	public function getRequestEvent(){
		return $this->request;
	}

	public function setRequestEvent( \Event\Request\Model $request ){
		$this->request	= $request;
		return $this;
	}
}
