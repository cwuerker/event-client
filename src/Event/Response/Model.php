<?php
namespace Event\Response;

class Model{

	protected $name;
	protected $request;
	protected $scopes	= array();
	protected $data		= NULL;
	protected $error	= NULL;

	public function __construct(){}

	public function getName(){
		return $this->name;
	}

	public function getData(){
		return $this->data;
	}

	public function getError(){
		return $this->error;
	}

	public function getRequest(){
		return $this->request;
	}

	public function setData( $data ){
		$this->data		= $data;
		return $this;
	}

	public function setError( $error ){
		$this->error	= $error;
		return $this;
	}

	public function setName( $name ){
		$this->name	= $name;
		return $this;
	}

	public function setRequest( \Event\Request\Model $request ){
		$this->request	= $request;
		return $this;
	}
}
