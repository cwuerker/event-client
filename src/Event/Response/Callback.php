<?php
namespace Event\Response;

class Callback{
	protected $callback		= array();
	protected $arguments	= array();
	protected $function		= NULL;

	public function __construct( $callback = NULL ){
		if( is_object( $callback ) /*&& $callback instanceof Closure */)
			$this->setCallbackByFunction( $callback );
		else if( is_array( $callback ) )
			$this->setCallback( $callback );
		return $this;
	}

	public static function create(){
		return new static;
	}

	public function handle( \Event\Response\Model $response ){
		if( !$this->callback )
			throw new \RuntimeException( 'No callback set' );
		return \call_user_func_array( $this->callback, array( $response ) );
	}

	public function setCallback( $object, $method ){
		$this->callback	= array( $object, $method );
		return $this;
	}

	public function setCallbackByFunction( $function ){
		$this->callback	= $function;
		return $this;
	}
}

class MyCallback extends Callback{

	public function __construct(){
		parent::__construct();
		$this->setCallback( $this, 'run' );
	}

	protected function run(){
		remark( 'MyCallback running...' );
	}
}
