<?php
namespace Event;

class Listener {

	protected $name;
	protected $callback;

	public function __construct(){}

	public static function create(){
		return new static;
	}

	public function getCallback(){
		return $this->callback;
	}

	public function getName(){
		return $this->name;
	}

	public function setCallback( \Event\Response\Callback $callback ){
		$this->callback	= $callback;
		return $this;
	}

	public function setName( $name ){
		$this->name	= $name;
		return $this;
	}
}
