<?php
namespace Event;

class Client {
	protected $clientType	= 'phpFrontend';
	protected $clientId		= '...';

	public function __construct( $host = '0.0.0.0', $port = 8001 ){
		$this->client	= new \CeusMedia\SocketStream\Client( $host, $port );

	}

	public function __destruct(){
		foreach( $this->listeners as $index => $listener ){
			if( $listener instanceof \Event\Response\Listener )
				$this->removeResponseListener( $listener );
			else
				$this->removeListener( $listener );
		}
	}

	public function addListener( \Event\Listener $listener ){
		$scopes		= array( 'name' => $listener->getName() );
		if( $listener instanceof \Event\Response\Listener ){
			$scopes	= $listener->getRequestEvent()->getScopes();
			$scopes['uuid']	= $listener->getRequestEvent()->getUuid();
		}
		$request	= \Event\Request::create( $this )
			->setName( 'registerListener' )
			->setScopes( $scopes );
		$this->sendRequest( $request );
		$this->listeners[]	= $listener;
	}

	public function getResponse( \Event\Request $request, $responseName, $function ){
		return $this->client->getResponse( $request->serialize() );
	}

	public function receiveRawResponse( $responseRaw ){
		$response	= new \Event\Response();
		$response->unserialize( $responseRaw );
		$name	= $response->getName();
		foreach( $this->listeners as $index => $listener ){
			if( $listener->getName() === $name ){
				$listener->getCallback()->handle( $response->getResponseEvent() );
				if( $listener instanceof \Event\Response\Listener )
					$this->removeResponseListener( $listener );
				else
					$this->removeListener( $listener );
			}
		}
	}

	public function removeListener( \Event\Listener $listener ){
		foreach( $this->listeners as $index => $item ){
			if( $item === $listener ){
				unset( $this->listeners[$index] );
				$request	= \Event\Request::create( $this )
					->setName( 'unregisterListener' )
					->setScopes( array(
						'name'	=> $listener->getName(),
					) );
				$this->sendRequest( $request );
			}
		}
	}

	public function removeResponseListener( \Event\Response\Listener $listener ){
		foreach( $this->listeners as $index => $item ){
			if( $item === $listener ){
				unset( $this->listeners[$index] );
				$request	= \Event\Request::create( $this )
					->setName( 'unregisterListener' )
					->setScopes( array(
						'uuid'	=> $listener->getRequestEvent()->getUuid(),
						'name'	=> $listener->getName(),
					) );
				$this->sendRequest( $request );
			}
		}
	}

	public function sendRequest( \Event\Request $request ){
		remark( 'Sending Request: ' );
		remark( $request->serialize() );
		return $this->client->getResponse( $request->serialize() );
	}
}
