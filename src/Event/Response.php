<?php
namespace Event;

class Response{

	public function __construct(){}

	public function getData(){
		return $this->model->getData();
	}

	public function getError(){
		return $this->model->getError();
	}

	public function getName(){
		return $this->model->getName();
	}

	public function getResponseEvent(){
		return $this->model;
	}

	public function unserialize( $response ){
		$response	= json_decode( $response );
//print_m( $response );
		$this->model	= new \Event\Response\Model;
		$this->model->setName( $response->name );
		$request	= \Event\Request\Model::create()
			->setName( $response->event->name )
			->setUuid( $response->event->uuid )
			->setScopes( $response->event->scopes );
		$this->model->setRequest( $request );
		if( !empty( $response->data ) )
			$this->model->setData( $response->data );
		if( !empty( $response->error ) )
			$this->model->setError( $response->error );
		return $this;
	}
}
