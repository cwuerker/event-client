# Event Client

## Installation

````composer install````

## Test

Starting a mocking event server for testing:
````
cd demo
php server.php
````

Running a demo client against the mocking event server:
````
cd demo
php client.php
````
